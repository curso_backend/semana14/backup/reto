from django.db import models
from django.contrib.auth.models import User
from django.contrib import admin

class Rol(models.Model):
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=100, blank=False, null=False)

    class Meta:
        verbose_name = 'Rol'
        verbose_name_plural = 'Roles'
        ordering = ['nombre']

    def __str__(self):
        return self.nombre

class Usuario(models.Model):
    id = models.AutoField(primary_key=True)
    nombres = models.CharField(max_length=150)
    apellidos = models.CharField(max_length=150)
    rol = models.ManyToManyField(Rol, through='RolUsuario', through_fields=('usuario_id', 'rol_id'))
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    class Meta:
        verbose_name = 'Usuario'
        verbose_name_plural = 'Usuarios'

    def __str__(self):
        return f'{self.nombres} {self.apellidos}'

class RolUsuario(models.Model):
    id = models.AutoField(primary_key=True)
    usuario_id = models.ForeignKey(Usuario, on_delete=models.CASCADE,)
    rol_id = models.ForeignKey(Rol, on_delete=models.CASCADE,)

    class Meta:
        verbose_name = 'Rol Usuario'
        verbose_name_plural = 'Roles Usuario'

    def __str__(self):
        return f'{self.usuario_id} / {self.rol_id}'

class RolUsuario_inline(admin.TabularInline):
    model = RolUsuario
    extra = 1

class UsuarioAdmin(admin.ModelAdmin):
    inlines = (RolUsuario_inline,)


class Vehiculo(models.Model):
    id = models.AutoField(primary_key=True)
    modelo = models.CharField(max_length=200, blank=False, null=False)
    marca = models.CharField(max_length=220, blank=False, null=False)
    año = models.CharField(max_length=220, blank=False, null=False)
    dueño = models.ForeignKey('Usuario', on_delete=models.CASCADE)


    class Meta:
        verbose_name = 'Vehículo'
        verbose_name_plural = 'Vehículos'
        ordering = ['dueño']

        
    def __str__(self):
        return f'{self.dueño} {self.marca} {self.modelo}'


class conductor_vehiculo(models.Model):
    id = models.AutoField(primary_key = True)
    conductor_id = models.ForeignKey(Usuario, null=False, blank=False, on_delete=models.CASCADE)
    vehiculo_id = models.ForeignKey(Vehiculo, null=False, blank=False, on_delete=models.CASCADE)
    activo= models.BooleanField(default=True)

    class Meta:
        verbose_name = 'conductor'
        verbose_name_plural = 'conductores'
        ordering = ['conductor_id']

    def __str__(self):
        cadena = "{0} => {1}"
        return cadena.format(self.conductor_id, self.vehiculo_id)


class direcciones(models.Model):
    id = models.AutoField(primary_key=True)
    direccion = models.CharField(max_length=120)
    tipos =(("D", "Destino"), ("O", "Origen"))
    tipo = models.CharField(max_length=1, choices=tipos, default="O")
    favorito=models.BooleanField(default=True)

    class Meta:
        verbose_name = 'direccion'
        verbose_name_plural = 'direcciones'
        ordering = ['tipo']

    def __str__(self):
        return self.tipo

class viaje(models.Model):
    id = models.AutoField(primary_key=True)
    conductor_id = models.ManyToManyField(Usuario)
    vehiculo_id = models.ForeignKey('Vehiculo', on_delete=models.CASCADE)
    origen=models.ForeignKey('direcciones', on_delete=models.CASCADE)
    #destino=models.ForeignKey('direcciones', on_delete=models.CASCADE)
    tarifa=models.CharField(max_length=120)

    class Meta:
        verbose_name = 'viaje'
        verbose_name_plural = 'viajes'
        ordering = ['origen']

    def __str__(self):
        return self.viaje
