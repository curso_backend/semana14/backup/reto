from django.contrib import admin
from .models import Usuario, Rol, RolUsuario, Vehiculo, conductor_vehiculo, direcciones, viaje


admin.site.register(Usuario)
admin.site.register(Rol)
admin.site.register(RolUsuario)
admin.site.register(Vehiculo)
admin.site.register(conductor_vehiculo)
admin.site.register(direcciones)
admin.site.register(viaje)


